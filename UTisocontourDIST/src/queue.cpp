/*****************************************************************************\
 *
 * queue.cc -- 
 *
 *
 * Author:      Fausto Bernardini (fxb@cs.purdue.edu)
 *
 * Created - June 15, 1993
 * Ported to C++ by Raymund Merkert - June 1995
 * Changes by Fausto Bernardini - Sept 1995 
 *
\*****************************************************************************/

// $Id: queue.cpp,v 1.3 2005/12/22 23:23:44 annao Exp $

#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <iostream>

#include "queue.h"


