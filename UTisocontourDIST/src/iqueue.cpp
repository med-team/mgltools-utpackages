/*****************************************************************************\
 *
 * squeue.cc -- 
 *
 *
 * Author:      Fausto Bernardini (fxb@cs.purdue.edu)
 *
 * Created - June 15, 1993
 * Ported to C++ by Raymund Merkert - June 1995
 * Changes by Fausto Bernardini - Sept 1995 
 *
\*****************************************************************************/

// $Id: iqueue.cpp,v 1.2 2005/11/11 23:02:20 annao Exp $

#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <iostream>

#include "squeue.h"


