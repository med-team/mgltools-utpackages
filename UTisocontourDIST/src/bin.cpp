/*****************************************************************************\
 *
 * bin.cc -- 
 *
 *
 * Author:      Fausto Bernardini (fxb@cs.purdue.edu)
 *
 * Created - June 15, 1993
 * Ported to C++ by Raymund Merkert - June 1995
 * Changes by Fausto Bernardini - Sept 1995 
 *
\*****************************************************************************/

// $Id: bin.cpp,v 1.1.1.1 2004/06/16 19:46:53 annao Exp $

#include <stdlib.h>
#include <string.h>
#include <assert.h>

#ifndef WIN32
#include <stream.h>
#endif

#include "bin.h"


