

/***********************************************************************************/
/*																				   */
/*	  Copyright 2003 University of Texas at Austin                                 */
/*	  Authors: Dr C Bajaj bajaj@cs.utexas.edu,                                     */
/*             S K Vinay  skvinay@cs.utexas.edu                                    */
/*             Anthony Thane thanea@ices.utexas.edu                                */
/*             Peter Djeu djeu@cs.utexas.edu                                       */
/*																				   */
/*    This program is free software; you can redistribute it and/or modify         */
/*    it under the terms of the GNU General Public License as published by         */
/*    the Free Software Foundation; either version 2 of the License, or            */
/*    (at your option) any later version.                                          */
/*																				   */
/*    This program is distributed in the hope that it will be useful,              */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*    GNU General Public License for more details.                                 */
/*																				   */
/*    You should have received a copy of the GNU General Public License			   */
/*    along with this program; if not, write to the Free Software                  */
/*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA    */
/*                                                                                 */
/***********************************************************************************/
// ImposterRenderer.cpp: implementation of the ImposterRenderer class.
//
//////////////////////////////////////////////////////////////////////

#include "ImposterRenderer.h"
#include "MyExtensions.h"

#include <stdlib.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

using CCVImposterRenderer::ImposterRenderer;
using CCVImposterRenderer::BallRenderer;
using CCVImposterRenderer::GaussianRenderer;
using CCVImposterRenderer::StickRenderer;
using CCVImposterRenderer::HelixRenderer;
using CCVImposterRenderer::HollowCylinderRenderer;

ImposterRenderer::ImposterRenderer()
{
	// m_Extensions = new MyExtensions();
	m_BallRenderer = new BallRenderer();
	m_GaussianRenderer = new GaussianRenderer();
	m_StickRenderer = new StickRenderer(&m_Extensions);
	m_HelixRenderer = new HelixRenderer(&m_Extensions);
	m_HollowCylinderRenderer = new HollowCylinderRenderer(&m_Extensions);

	initialized = false;
	m_ShaderContext = 0;
}

ImposterRenderer::~ImposterRenderer()
{
	if( m_ShaderContext ) cgDestroyContext(m_ShaderContext);

	delete m_HollowCylinderRenderer; m_HollowCylinderRenderer = 0;
	delete m_HelixRenderer; m_HelixRenderer = 0;
	delete m_StickRenderer; m_StickRenderer = 0;
	delete m_GaussianRenderer; m_GaussianRenderer = 0;
	delete m_BallRenderer; m_BallRenderer = 0;
	// delete m_Extensions; m_Extensions = 0;
}

void cgErrorCallback(void)
{
    CGerror LastError = cgGetError();
}

void ImposterRenderer::initSubRenderers( bool drawFunction )
{
	m_BallRenderer->initRenderer( drawFunction );
	m_GaussianRenderer->initRenderer( drawFunction );
	m_StickRenderer->initRenderer( drawFunction );
	m_HelixRenderer->initRenderer( drawFunction );
	m_HollowCylinderRenderer->initRenderer( drawFunction );
}

bool ImposterRenderer::initRenderer()
{
	// dont need to return false, change SKVINAY
	cgSetErrorCallback(cgErrorCallback);
	if( !m_Extensions.initExtensions(
                    "GL_VERSION_1_2 "
                    "GL_EXT_secondary_color "
                    "GL_ARB_multitexture "
                    "GL_ARB_vertex_buffer_object" ))
		return false;

	initialized = true;
	return true;
}

void ImposterRenderer::renderBuffer( bool useHardwareBuffer, bool useFunctionOnSurface, double* rotations, double* translations, int numberOfTransformations, bool randomizeColors, float avgradius  )
{
	m_BallRenderer->renderBuffer(useHardwareBuffer, useFunctionOnSurface, rotations, translations, numberOfTransformations, randomizeColors, avgradius);
	m_GaussianRenderer->renderBuffer();
	m_StickRenderer->renderBuffer(useHardwareBuffer, useFunctionOnSurface, rotations, translations, numberOfTransformations, randomizeColors);
	m_HelixRenderer->renderBuffer(useHardwareBuffer, useFunctionOnSurface, rotations, translations, numberOfTransformations, randomizeColors);
	m_HollowCylinderRenderer->renderBuffer(useHardwareBuffer, useFunctionOnSurface, rotations, translations, numberOfTransformations, randomizeColors);
}

bool ImposterRenderer::addFunction( unsigned char* data, double minx, double miny, double minz, double maxx, double maxy, double maxz, int width, int height, int depth)
{
	return m_BallRenderer->addFunction(data, minx, miny, minz, maxx, maxy, maxz, width, height, depth);
}

void ImposterRenderer::clear()
{
	m_BallRenderer->clearBalls();
	m_GaussianRenderer->clearGaussians();
	m_StickRenderer->clearSticks();
	m_HelixRenderer->clear();
	m_HollowCylinderRenderer->clear();
}

void ImposterRenderer::forceMeshRendering(bool status) {
        BallRenderer::forceMeshRendering(status);
	GaussianRenderer::forceMeshRendering(status);
	StickRenderer::forceMeshRendering(status);
	HelixRenderer::forceMeshRendering(status);
	HollowCylinderRenderer::forceMeshRendering(status);
}
