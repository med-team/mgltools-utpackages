

/***********************************************************************************/
/*																				   */
/*	  Copyright 2003 University of Texas at Austin                                 */
/*	  Authors: Dr C Bajaj bajaj@cs.utexas.edu,                                     */
/*             Anthony Thane thanea@ices.utexas.edu                                */
/*																				   */
/*    This program is free software; you can redistribute it and/or modify         */
/*    it under the terms of the GNU General Public License as published by         */
/*    the Free Software Foundation; either version 2 of the License, or            */
/*    (at your option) any later version.                                          */
/*																				   */
/*    This program is distributed in the hope that it will be useful,              */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*    GNU General Public License for more details.                                 */
/*																				   */
/*    You should have received a copy of the GNU General Public License			   */
/*    along with this program; if not, write to the Free Software                  */
/*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA    */
/*                                                                                 */
/***********************************************************************************/

// GlobalCGContext.cpp: implementation of the GlobalCGContext class.
//
//////////////////////////////////////////////////////////////////////

#include "GlobalCGContext.h"
#include <stdio.h>

using CCVImposterRenderer::GlobalCGContext;

GlobalCGContext* GlobalCGContext::ms_GlobalCGContext = 0;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

GlobalCGContext::GlobalCGContext()
{
	m_ShaderContext = cgCreateContext();
	cgSetErrorCallback(cgErrorCallback);
}

GlobalCGContext::~GlobalCGContext()
{
	cgDestroyContext(m_ShaderContext);
}

CGcontext GlobalCGContext::getCGContext()
{
	if (ms_GlobalCGContext == 0) {
		initGlobalPointer();
		return ms_GlobalCGContext->m_ShaderContext;
	}
	else {
		return ms_GlobalCGContext->m_ShaderContext;
	}
}

void GlobalCGContext::initGlobalPointer()
{
	ms_GlobalCGContext = new GlobalCGContext;
}

void GlobalCGContext::cgErrorCallback(void)
{
	const char* error = cgGetErrorString(cgGetError());
//	qDebug("Error: %s", error);
	printf("Error: %s", error);
}

