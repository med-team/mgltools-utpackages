

/***********************************************************************************/
/*																				   */
/*	  Copyright 2003 University of Texas at Austin                                 */
/*	  Authors: Dr C Bajaj bajaj@cs.utexas.edu,                                     */
/*             S K Vinay  skvinay@cs.utexas.edu                                    */
/*             Anthony Thane thanea@ices.utexas.edu                                */
/*																				   */
/*    This program is free software; you can redistribute it and/or modify         */
/*    it under the terms of the GNU General Public License as published by         */
/*    the Free Software Foundation; either version 2 of the License, or            */
/*    (at your option) any later version.                                          */
/*																				   */
/*    This program is distributed in the hope that it will be useful,              */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*    GNU General Public License for more details.                                 */
/*																				   */
/*    You should have received a copy of the GNU General Public License			   */
/*    along with this program; if not, write to the Free Software                  */
/*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA    */
/*                                                                                 */
/***********************************************************************************/
// FunctionSphereRenderer.h: interface for the FunctionSphereRenderer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FUNCTIONSPHERERENDERER_H__6F47A833_5065_4885_A0C3_B44CF7530782__INCLUDED_)
#define AFX_FUNCTIONSPHERERENDERER_H__6F47A833_5065_4885_A0C3_B44CF7530782__INCLUDED_

#include <Cg/cgGL.h>

namespace CCVImposterRenderer {
	
	class Texture;

	class FunctionSphereRenderer  
	{
	public:
		FunctionSphereRenderer();
		virtual ~FunctionSphereRenderer();

		bool bindProgramAndParams();
		bool unbindProgramAndParams();
		bool initCG();
		bool loadProgram();
                bool fallbackLoadProgram();
		bool addFunction( unsigned char* data, double minx, double miny, double minz, double maxx, double maxy, double maxz, int width, int height, int depth);
		void bindMatrices();
        protected:
		CGprofile m_VertexProfile;
		CGprofile m_FragmentProfile;

		CGprogram m_VertexProgram;
		CGprogram m_FragmentProgram;
		CGparameter m_PositionParam;
		CGparameter m_ColorParam;
		CGparameter m_ModelViewITParam;
		CGparameter m_ModelViewInverseParam;
		CGparameter m_ModelViewProjParam;
		CGparameter m_ProjParam;
		CGparameter m_ModelViewProjInverseParam;

		CGparameter m_MainColorParam;
		CGparameter m_MinExtentParam;
		CGparameter m_MaxExtentParam;
		CGparameter m_NormalMapParam;
		CGparameter m_DepthMapParam;
		CGparameter m_FunctionMapParam;

		Texture* m_TextureNormalMap;
		Texture* m_TextureDepthMap;
		Texture* m_TextureFunctionMap;

		unsigned char* m_Data;
		double m_minx;
		double m_miny;
		double m_minz;
		double m_maxx;
		double m_maxy;
		double m_maxz;
		int m_Width;
		int m_Height;
		int m_Depth;
	};

};

#endif // !defined(AFX_FUNCTIONSPHERERENDERER_H__6F47A833_5065_4885_A0C3_B44CF7530782__INCLUDED_)
