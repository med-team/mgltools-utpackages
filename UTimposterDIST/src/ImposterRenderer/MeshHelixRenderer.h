/*
 * A fallback replacement for the CG-Impostor system currently used to draw Helices.
 * This class will read the CG-encoded data and interpret it to draw Mesh instances.
 *
 * Note that per-pixel effects like those provided by the shader are not feasible with this
 * system.
 *
 * m_strange@mail.utexas.edu
 * 
 * */

#ifndef __MESHHELIXRENDERER_H__
#define __MESHHELIXRENDERER_H__

#if defined(__APPLE__)
# include <gl.h>
# include <glu.h>
#else
# include <GL/gl.h>
# include <GL/glu.h>
#endif

#include <stdio.h>
#include "ExpandableBuffer.h"

class MeshHelixRenderer {
 public:
        /* this function uses gluHelix to draw the actual components */
        static void DrawHelixBuffer(const ExpandableBuffer<GLfloat>& position,
                                       const ExpandableBuffer<GLfloat>& color,
                                       const ExpandableBuffer<GLfloat>& textureCoord1,
                                       const ExpandableBuffer<GLfloat>& textureCoord2);
 private:
        static GLUquadric *m_Quadric;
        static const int LEVEL_OF_DETAIL;
        static const GLfloat HEIGHT_STEP;
        static const GLfloat STRIP_HEIGHT;
        static const GLfloat HEIGHT_TO_COILS;
};

#endif
