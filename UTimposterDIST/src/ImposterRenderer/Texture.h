

/***********************************************************************************/
/*																				   */
/*	  Copyright 2003 University of Texas at Austin                                 */
/*	  Authors: Dr C Bajaj bajaj@cs.utexas.edu,                                     */
/*             S K Vinay  skvinay@cs.utexas.edu                                    */
/*             Anthony Thane thanea@ices.utexas.edu                                */
/*																				   */
/*    This program is free software; you can redistribute it and/or modify         */
/*    it under the terms of the GNU General Public License as published by         */
/*    the Free Software Foundation; either version 2 of the License, or            */
/*    (at your option) any later version.                                          */
/*																				   */
/*    This program is distributed in the hope that it will be useful,              */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*    GNU General Public License for more details.                                 */
/*																				   */
/*    You should have received a copy of the GNU General Public License			   */
/*    along with this program; if not, write to the Free Software                  */
/*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA    */
/*                                                                                 */
/***********************************************************************************/

// Texture.h: interface for the Texture class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXTURE_H__BC94627F_0E50_4516_A138_B7E672BEDE4E__INCLUDED_)
#define AFX_TEXTURE_H__BC94627F_0E50_4516_A138_B7E672BEDE4E__INCLUDED_

class MyExtensions;

namespace CCVImposterRenderer {

	class Texture  
	{
	public:
		Texture();
		virtual ~Texture();

		void loadPGM(const char* name);
		void loadPPM(const char* name);
		bool loadFunction(unsigned char* m_Data, int w, int h, int d);

		void calculateSphereOpacityAndNormalMap( int size );
		void calculateNormalMap(unsigned int w, unsigned int h);
		bool calculateHelixOpacityMap(unsigned int w, unsigned int h);
		void calculateCylinderDepthMap(unsigned int w);

		void calculateCylinderNormalMap(unsigned int w);
		void calculateShadingLookupTable(int w, int h, double shininess);

		void calculateDepthMap(int w, int h);

		unsigned int getTextureID() const;

		void bindTexture( int dim ) const;

	protected:
		bool init3DTexture();

	private:
		unsigned int m_TextureID;
		MyExtensions* m_Extensions;
		int m_Texture3dPresent;
	};

};

#endif // !defined(AFX_TEXTURE_H__BC94627F_0E50_4516_A138_B7E672BEDE4E__INCLUDED_)
