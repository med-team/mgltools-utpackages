

/***********************************************************************************/
/*																				   */
/*	  Copyright 2003 University of Texas at Austin                                 */
/*	  Authors: Dr C Bajaj bajaj@cs.utexas.edu,                                     */
/*             S K Vinay  skvinay@cs.utexas.edu                                    */
/*             Anthony Thane thanea@ices.utexas.edu                                */
/*             Peter Djeu djeu@cs.utexas.edu                                       */
/*																				   */
/*    This program is free software; you can redistribute it and/or modify         */
/*    it under the terms of the GNU General Public License as published by         */
/*    the Free Software Foundation; either version 2 of the License, or            */
/*    (at your option) any later version.                                          */
/*																				   */
/*    This program is distributed in the hope that it will be useful,              */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*    GNU General Public License for more details.                                 */
/*																				   */
/*    You should have received a copy of the GNU General Public License			   */
/*    along with this program; if not, write to the Free Software                  */
/*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA    */
/*                                                                                 */
/***********************************************************************************/
// ImposterRenderer.h: interface for the ImposterRenderer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IMPOSTERRENDERER_H__01E3AE03_D9AD_4A30_A364_BA19EF5ADEFE__INCLUDED_)
#define AFX_IMPOSTERRENDERER_H__01E3AE03_D9AD_4A30_A364_BA19EF5ADEFE__INCLUDED_

class MyExtensions;
class VolumeData;

#include "Texture.h"
#include "BallRenderer.h"
#include "GaussianRenderer.h"
#include "StickRenderer.h"
#include "HelixRenderer.h"
#include "HollowCylinderRenderer.h"
#include "sphereDrawing.h"
#include <Cg/cgGL.h>
#include <GL/gl.h>

namespace CCVImposterRenderer {

	class ImposterRenderer  
	{
	public:
		ImposterRenderer();
		virtual ~ImposterRenderer();
		bool initRenderer();
		void renderBuffer( bool useHardwareBuffer, bool useFunctionOnSurface, double* rotations, double* translations, int numberOfTransformations, bool randomizeColors, float avgradius );
		void initSubRenderers( bool drawFunction );
		bool addFunction( unsigned char* data, double minx, double miny, double minz, double maxx, double maxy, double maxz, int width, int height, int depth);
		//void drawFunctionOnSurfaces( bool drawFunction );
		void clear();

		BallRenderer* m_BallRenderer;
		GaussianRenderer* m_GaussianRenderer;
		StickRenderer* m_StickRenderer;
		HelixRenderer* m_HelixRenderer;
		HollowCylinderRenderer* m_HollowCylinderRenderer;

                /* force using OpenGL meshes instead of the CG impostors */
                static void forceMeshRendering(bool status);
	protected:

		bool initialized;

		// general cg
		CGcontext m_ShaderContext;
		CGprofile m_VertexProfile;
		CGprofile m_FragmentProfile;

		/* MyExtensions* m_Extensions; */
                MyExtensions m_Extensions;
	};

};

#endif // !defined(AFX_IMPOSTERRENDERER_H__01E3AE03_D9AD_4A30_A364_BA19EF5ADEFE__INCLUDED_)
