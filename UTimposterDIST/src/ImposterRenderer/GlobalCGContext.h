

/***********************************************************************************/
/*																				   */
/*	  Copyright 2003 University of Texas at Austin                                 */
/*	  Authors: Dr C Bajaj bajaj@cs.utexas.edu,                                     */
/*             Anthony Thane thanea@ices.utexas.edu                                */
/*																				   */
/*    This program is free software; you can redistribute it and/or modify         */
/*    it under the terms of the GNU General Public License as published by         */
/*    the Free Software Foundation; either version 2 of the License, or            */
/*    (at your option) any later version.                                          */
/*																				   */
/*    This program is distributed in the hope that it will be useful,              */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*    GNU General Public License for more details.                                 */
/*																				   */
/*    You should have received a copy of the GNU General Public License			   */
/*    along with this program; if not, write to the Free Software                  */
/*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA    */
/*                                                                                 */
/***********************************************************************************/

// GlobalCGContext.h: interface for the GlobalCGContext class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GLOBALCGCONTEXT_H__37EBD53C_2758_4A18_9DD1_7FDF0DB7D705__INCLUDED_)
#define AFX_GLOBALCGCONTEXT_H__37EBD53C_2758_4A18_9DD1_7FDF0DB7D705__INCLUDED_

#include <Cg/cgGL.h>

namespace CCVImposterRenderer {

	class GlobalCGContext  
	{
	public:
		virtual ~GlobalCGContext();

		static CGcontext getCGContext();

	protected:
		GlobalCGContext();
		CGcontext m_ShaderContext;

		static void initGlobalPointer();

		static void cgErrorCallback(void);
		static GlobalCGContext* ms_GlobalCGContext;

	};

};

#endif // !defined(AFX_GLOBALCGCONTEXT_H__37EBD53C_2758_4A18_9DD1_7FDF0DB7D705__INCLUDED_)
