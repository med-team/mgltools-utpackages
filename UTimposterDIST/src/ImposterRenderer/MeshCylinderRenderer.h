/*
 * A fallback replacement for the CG-Impostor system currently used to draw Cylinders.
 * This class will read the CG-encoded data and interpret it to draw Mesh instances.
 *
 * Note that per-pixel effects like those provided by the shader are not feasible with this
 * system.
 *
 * m_strange@mail.utexas.edu
 * 
 * */

#ifndef __MESHCYLINDERRENDERER_H__
#define __MESHCYLINDERRENDERER_H__

#if defined(__APPLE__)
# include <gl.h>
# include <glu.h>
#else
# include <GL/gl.h>
# include <GL/glu.h>
#endif

#include <stdio.h>
#include "ExpandableBuffer.h"

class MeshCylinderRenderer {
 public:
        /* this function uses gluCylinder to draw the actual components */
        static void DrawCylinderBuffer(const ExpandableBuffer<GLfloat>& position,
                                       const ExpandableBuffer<GLfloat>& color,
                                       const ExpandableBuffer<GLfloat>& textureCoord1,
                                       const ExpandableBuffer<GLfloat>& textureCoord2);
 private:
        static GLUquadric *m_Quadric;
        static const int LEVEL_OF_DETAIL;
};

#endif
