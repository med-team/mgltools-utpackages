

/***********************************************************************************/
/*																				   */
/*	  Copyright 2003 University of Texas at Austin                                 */
/*	  Authors: Dr C Bajaj bajaj@cs.utexas.edu,                                     */
/*             S K Vinay  skvinay@cs.utexas.edu                                    */
/*             Anthony Thane thanea@ices.utexas.edu                                */
/*																				   */
/*    This program is free software; you can redistribute it and/or modify         */
/*    it under the terms of the GNU General Public License as published by         */
/*    the Free Software Foundation; either version 2 of the License, or            */
/*    (at your option) any later version.                                          */
/*																				   */
/*    This program is distributed in the hope that it will be useful,              */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*    GNU General Public License for more details.                                 */
/*																				   */
/*    You should have received a copy of the GNU General Public License			   */
/*    along with this program; if not, write to the Free Software                  */
/*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA    */
/*                                                                                 */
/***********************************************************************************/

// BallRenderer.h: interface for the BallRenderer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BALLRENDERER_H__18AC0DE6_49B9_4036_9221_63B5DF373C4B__INCLUDED_)
#define AFX_BALLRENDERER_H__18AC0DE6_49B9_4036_9221_63B5DF373C4B__INCLUDED_

#include <Cg/cgGL.h>
#include <stdio.h>
#include "ExpandableBuffer.h"
#include "MyExtensions.h"

namespace CCVImposterRenderer {

	class PlainSphereRenderer;
	class FunctionSphereRenderer;

	class BallRenderer  
	{
	public:
		BallRenderer();
		virtual ~BallRenderer();

		void clearBalls();
		inline void addBall(float x, float y, float z, float radius, float red, float green, float blue);
		int getNumBalls() const;
		void initRenderer( bool drawFunction );
		void renderBuffer( bool useHardwareBuffer, bool useFunctionOnSurface, double* rotations, double* translations, int numberOfTransformations, bool randomizeColors, float avgradius );
		bool addFunction( unsigned char* data, double minx, double miny, double minz, double maxx, double maxy, double maxz, int width, int height, int depth);
                static void forceMeshRendering(bool status) { m_UseGLFallback = status; }
	protected:
		bool initCG();
		bool initExtensions();
		void initDefaults();
		void prepareBuffers();

		void allocateBufferObjects(unsigned int num);
		void deallocateBufferObjects();
		void renderOnce();

		// the buffers
		ExpandableBuffer<GLfloat> m_TextureCoord;
		ExpandableBuffer<GLfloat> m_Position;
		ExpandableBuffer<GLfloat> m_Color;

		bool m_Dirty;
		unsigned int m_NumBalls;
		unsigned int m_NumBufferObjectsAllocated;
		GLuint* m_BufferObjects;
		MyExtensions* m_Extensions;
		int m_CGRequirements;
		int m_OpenGLRequirements;
                
		bool m_Initialized;
                static bool m_UseGLFallback;
                
		int m_ProgramsLoaded;
		PlainSphereRenderer* m_PlainSphereRenderer;
		FunctionSphereRenderer* m_FunctionSphereRenderer;
	};

	inline void BallRenderer::addBall(float x, float y, float z, float radius, float red, float green, float blue)
	{
		//if( x > 0 ) return;
		//if( y < 65 ) return;
		//red = 1;
		//green = 0;
		//blue = 0;

	/*	static FILE* fp;
		static fileOpened = false;
		if( !fileOpened )
		{
			fp = fopen("Atoms.txt", "a");
			fileOpened = true;
		}
		fprintf( fp, "%f %f %f %f\n", x, y, z, radius );
	*/
	/*	{
			red = 0.5;
			green = 0;
			blue = 0.25;
			static int addedNumber = 0;
			if( addedNumber == 2 ) return;
			addedNumber++;
			x = 0 + addedNumber*2.0;
			y = 0;
			z = 0;
			radius = 2.0f;
		}
	*/
		m_Dirty = true;
		bool result = true;
		// position
		result &= m_Position.add(x);
		result &= m_Position.add(y);
		result &= m_Position.add(z);
		result &= m_Position.add(0);
		result &= m_Position.add(x);
		result &= m_Position.add(y);
		result &= m_Position.add(z);
		result &= m_Position.add(1);
		result &= m_Position.add(x);
		result &= m_Position.add(y);
		result &= m_Position.add(z);
		result &= m_Position.add(2);
		result &= m_Position.add(x);
		result &= m_Position.add(y);
		result &= m_Position.add(z);
		result &= m_Position.add(3);

		// color
		result &= m_Color.add(red);
		result &= m_Color.add(green);
		result &= m_Color.add(blue);
		result &= m_Color.add(radius); //0.1f);

		result &= m_Color.add(red);
		result &= m_Color.add(green);
		result &= m_Color.add(blue);
		result &= m_Color.add(radius); //0.6f);

		result &= m_Color.add(red);
		result &= m_Color.add(green);
		result &= m_Color.add(blue);
		result &= m_Color.add(radius); //0.1f);

		result &= m_Color.add(red);
		result &= m_Color.add(green);
		result &= m_Color.add(blue);
		result &= m_Color.add(radius); //0.1f);

		// texturecoord/offset
		/*
		result &= m_TextureCoord.add(-1);
		result &= m_TextureCoord.add(-1);
		result &= m_TextureCoord.add(0.0);
		result &= m_TextureCoord.add(radius);

		result &= m_TextureCoord.add(1);
		result &= m_TextureCoord.add(-1);
		result &= m_TextureCoord.add(0.0);
		result &= m_TextureCoord.add(radius);

		result &= m_TextureCoord.add(1);
		result &= m_TextureCoord.add(1);
		result &= m_TextureCoord.add(0.0);
		result &= m_TextureCoord.add(radius);

		result &= m_TextureCoord.add(-1);
		result &= m_TextureCoord.add(1);
		result &= m_TextureCoord.add(0.0);
		result &= m_TextureCoord.add(radius);
		*/

		if (!result) {
			printf("Warning, could not add ball");
		}

		m_NumBalls++;
	}

};

#endif // !defined(AFX_BALLRENDERER_H__18AC0DE6_49B9_4036_9221_63B5DF373C4B__INCLUDED_)
