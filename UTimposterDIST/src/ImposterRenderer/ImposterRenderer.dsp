# Microsoft Developer Studio Project File - Name="ImposterRenderer" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=ImposterRenderer - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "ImposterRenderer.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ImposterRenderer.mak" CFG="ImposterRenderer - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ImposterRenderer - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "ImposterRenderer - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "ImposterRenderer - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "$(CG_INC_PATH)" /I "../UsefulMath" /I "../OpenGL_Viewer" /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /FD /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "ImposterRenderer - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "$(CG_INC_PATH)" /I "../UsefulMath" /I "../OpenGL_Viewer" /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /D "QT_DLL" /D "QT_THREAD_SUPPORT" /FD /GZ /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "ImposterRenderer - Win32 Release"
# Name "ImposterRenderer - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\BallRenderer.cpp
# End Source File
# Begin Source File

SOURCE=.\FunctionSphereRenderer.cpp
# End Source File
# Begin Source File

SOURCE=.\GaussianRenderer.cpp
# End Source File
# Begin Source File

SOURCE=.\GlobalCGContext.cpp
# End Source File
# Begin Source File

SOURCE=.\HelixRenderer.cpp
# End Source File
# Begin Source File

SOURCE=.\HollowCylinderRenderer.cpp
# End Source File
# Begin Source File

SOURCE=.\ImposterRenderer.cpp
# End Source File
# Begin Source File

SOURCE=.\PlainSphereRenderer.cpp
# End Source File
# Begin Source File

SOURCE=.\sphereDrawing.cpp
# End Source File
# Begin Source File

SOURCE=.\StickRenderer.cpp
# End Source File
# Begin Source File

SOURCE=.\Texture.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\BallRenderer.h
# End Source File
# Begin Source File

SOURCE=.\ExpandableBuffer.h
# End Source File
# Begin Source File

SOURCE=.\FunctionSphereRenderer.h
# End Source File
# Begin Source File

SOURCE=.\GaussianRenderer.h
# End Source File
# Begin Source File

SOURCE=.\GlobalCGContext.h
# End Source File
# Begin Source File

SOURCE=.\HelixRenderer.h
# End Source File
# Begin Source File

SOURCE=.\HollowCylinderRenderer.h
# End Source File
# Begin Source File

SOURCE=.\ImposterRenderer.h
# End Source File
# Begin Source File

SOURCE=.\PlainSphereRenderer.h
# End Source File
# Begin Source File

SOURCE=.\sphereDrawing.h
# End Source File
# Begin Source File

SOURCE=.\StickRenderer.h
# End Source File
# Begin Source File

SOURCE=.\Texture.h
# End Source File
# End Group
# Begin Group "CG Files"

# PROP Default_Filter "*.cg"
# Begin Source File

SOURCE=..\cgGLCylFragmentShader.cg
# End Source File
# Begin Source File

SOURCE=.\cgGLCylFragmentShader.h
# End Source File
# Begin Source File

SOURCE=..\cgGLCylVertexShader.cg
# End Source File
# Begin Source File

SOURCE=.\cgGLCylVertexShader.h
# End Source File
# Begin Source File

SOURCE=..\cgGLFragmentFunctionShader.cg
# End Source File
# Begin Source File

SOURCE=.\cgGLFragmentFunctionShader.h
# End Source File
# Begin Source File

SOURCE=..\cgGLFragmentShader.cg
# End Source File
# Begin Source File

SOURCE=.\cgGLFragmentShader.h
# End Source File
# Begin Source File

SOURCE=..\cgGLHelixFragmentShader.cg
# End Source File
# Begin Source File

SOURCE=.\cgGLHelixFragmentShader.h
# End Source File
# Begin Source File

SOURCE=..\cgGLHelixVertexShader.cg
# End Source File
# Begin Source File

SOURCE=.\cgGLHelixVertexShader.h
# End Source File
# Begin Source File

SOURCE=..\cgGLHollowCylinderFragmentShader.cg
# End Source File
# Begin Source File

SOURCE=.\cgGLHollowCylinderFragmentShader.h
# End Source File
# Begin Source File

SOURCE=..\cgGLHollowCylinderVertexShader.cg
# End Source File
# Begin Source File

SOURCE=.\cgGLHollowCylinderVertexShader.h
# End Source File
# Begin Source File

SOURCE=..\cgGLVertexFunctionShader.cg
# End Source File
# Begin Source File

SOURCE=.\cgGLVertexFunctionShader.h
# End Source File
# Begin Source File

SOURCE=..\cgGLVertexShader.cg
# End Source File
# Begin Source File

SOURCE=.\cgGLVertexShader.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\ImposterRenderer.pro
# End Source File
# End Target
# End Project
