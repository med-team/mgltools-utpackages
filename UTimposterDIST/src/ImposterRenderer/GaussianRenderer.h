/*****************************************************************************/
/*                             ______________________                        */
/*                            / _ _ _ _ _ _ _ _ _ _ _)                       */
/*            ____  ____  _  / /__  __  _____  __                            */
/*           (_  _)( ___)( \/ /(  \/  )(  _  )(  )                           */
/*             )(   )__)  )  (  )    (  )(_)(  )(__                          */
/*            (__) (____)/ /\_)(_/\/\_)(_____)(____)                         */
/*            _ _ _ _ __/ /                                                  */
/*           (___________/                     ___  ___                      */
/*                                      \  )| |   ) _ _|\   )                */
/*                                 ---   \/ | |  / |___| \_/                 */
/*                                                       _/                  */
/*                                                                           */
/*   Copyright (C) The University of Texas at Austin                         */
/*                                                                           */
/*     Author:     Vinay Siddavanahalli <skvinay@cs.utexas.edu>   2004-2005  */
/*                                                                           */
/*     Principal Investigator: Chandrajit Bajaj <bajaj@ices.utexas.edu>      */
/*                                                                           */
/*         Professor of Computer Sciences,                                   */
/*         Computational and Applied Mathematics Chair in Visualization,     */
/*         Director, Computational Visualization Center (CVC),               */
/*         Institute of Computational Engineering and Sciences (ICES)        */
/*         The University of Texas at Austin,                                */
/*         201 East 24th Street, ACES 2.324A,                                */
/*         1 University Station, C0200                                       */
/*         Austin, TX 78712-0027                                             */
/*         http://www.cs.utexas.edu/~bajaj                                   */
/*                                                                           */
/*         http://www.ices.utexas.edu/CVC                                    */
/*  This software comes with a license. Using this code implies that you     */
/*  read, understood and agreed to all the terms and conditions in that      */
/*  license.                                                                 */
/*                                                                           */
/*  We request that you agree to acknowledge the use of the software that    */
/*  results in any published work, including scientific papers, films and    */
/*  videotapes by citing the reference listed below                          */
/*                                                                           */
/*    C. Bajaj, P. Djeu, V. Siddavanahalli, A. Thane,                        */
/*    Interactive Visual Exploration of Large Flexible Multi-component       */
/*    Molecular Complexes,                                                   */
/*    Proc. of the Annual IEEE Visualization Conference, October 2004,       */
/*    Austin, Texas, IEEE Computer Society Press, pp. 243-250.               */
/*                                                                           */
/*****************************************************************************/
// GaussianRenderer.h: interface for the GaussianRenderer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GAUSSIANRENDERER_H__B61DC823_49F9_4912_BE8E_6E4AABE7F0CB__INCLUDED_)
#define AFX_GAUSSIANRENDERER_H__B61DC823_49F9_4912_BE8E_6E4AABE7F0CB__INCLUDED_

#include <Cg/cgGL.h>
#include <stdio.h>
#include "ExpandableBuffer.h"
#include "MyExtensions.h"

namespace CCVImposterRenderer {

	class PlainSphereRenderer;
	class FunctionSphereRenderer;
	class Texture;

	class GaussianRenderer  
	{
	public:
		GaussianRenderer();
		virtual ~GaussianRenderer();

		void clearGaussians();
		inline void addGaussian(float x, float y, float z, float radius, float red, float green, float blue);
		int getNumGaussians() const;
		void initRenderer( bool drawFunction );
		void renderBuffer();
                static void forceMeshRendering(bool status) { m_UseGLFallback = status; }
	protected:
		bool initCG();
		bool initExtensions();
		bool loadProgram();
		void prepareBuffers();

		bool bindProgramAndParams();
		bool unbindProgramAndParams();
		void bindMatrices();

		void allocateBufferObjects(unsigned int num);
		void deallocateBufferObjects();
		void renderOnce();

		bool m_Initialized;
                static bool m_UseGLFallback;
                
		// the buffers
		ExpandableBuffer<GLfloat> m_TextureCoord;
		ExpandableBuffer<GLfloat> m_Position;
		ExpandableBuffer<GLfloat> m_Color;

		bool m_Dirty;
		unsigned int m_NumGaussians;
		unsigned int m_NumBufferObjectsAllocated;
		GLuint* m_BufferObjects;
		MyExtensions* m_Extensions;
		int m_CGRequirements;
		int m_OpenGLRequirements;
		int m_ProgramsLoaded;

		CGprogram m_VertexProgram;
		CGprogram m_FragmentProgram;
		CGprofile m_VertexProfile;
		CGprofile m_FragmentProfile;
		CGparameter m_PositionParam;
		CGparameter m_ColorParam;
		CGparameter m_ModelViewITParam;
		CGparameter m_ModelViewInverseParam;
		CGparameter m_ModelViewProjParam;
		CGparameter m_ProjParam;

		CGparameter m_MainColorParam;
		CGparameter m_NormalMapParam;
		CGparameter m_DepthMapParam;
		Texture* m_TextureNormalMap;
		Texture* m_TextureDepthMap;

	};

	inline void GaussianRenderer::addGaussian(float x, float y, float z, float radius, float red, float green, float blue)
	{
		m_Dirty = true;
		bool result = true;
		// position
		result &= m_Position.add(x);
		result &= m_Position.add(y);
		result &= m_Position.add(z);
		result &= m_Position.add(0);
		result &= m_Position.add(x);
		result &= m_Position.add(y);
		result &= m_Position.add(z);
		result &= m_Position.add(1);
		result &= m_Position.add(x);
		result &= m_Position.add(y);
		result &= m_Position.add(z);
		result &= m_Position.add(2);
		result &= m_Position.add(x);
		result &= m_Position.add(y);
		result &= m_Position.add(z);
		result &= m_Position.add(3);

		// color
		result &= m_Color.add(red);
		result &= m_Color.add(green);
		result &= m_Color.add(blue);
		result &= m_Color.add(radius); //0.1f);

		result &= m_Color.add(red);
		result &= m_Color.add(green);
		result &= m_Color.add(blue);
		result &= m_Color.add(radius); //0.6f);

		result &= m_Color.add(red);
		result &= m_Color.add(green);
		result &= m_Color.add(blue);
		result &= m_Color.add(radius); //0.1f);

		result &= m_Color.add(red);
		result &= m_Color.add(green);
		result &= m_Color.add(blue);
		result &= m_Color.add(radius); //0.1f);

		if (!result) {
			printf("Warning, could not add gaussian");
		}

		m_NumGaussians++;
	}

};


#endif // !defined(AFX_GAUSSIANRENDERER_H__B61DC823_49F9_4912_BE8E_6E4AABE7F0CB__INCLUDED_)
