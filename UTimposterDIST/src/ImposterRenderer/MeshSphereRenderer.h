/*
 * A fallback replacement for the CG-Impostor system currently used to draw Spheres.
 * This class will read the CG-encoded data and interpret it to draw Mesh instances.
 *
 * Note that per-pixel effects like those provided by the shader are not feasible with this
 * system.
 *
 * m_strange@mail.utexas.edu
 * 
 * */

#ifndef __MESHSPHERERENDERER_H__
#define __MESHSPHERERENDERER_H__

#if defined(__APPLE__)
# include <gl.h>
# include <glu.h>
#else
# include <GL/gl.h>
# include <GL/glu.h>
#endif

#include <stdio.h>
#include "ExpandableBuffer.h"

class MeshSphereRenderer {
 public:
        /* this function uses gluSphere to draw the actual components */
        static void DrawSphereBuffer(const ExpandableBuffer<GLfloat>& position,
                                     const ExpandableBuffer<GLfloat>& color);
        /* levelOfDetail refers to the slice/stack count used when drawing the sphere */
        /* static void DrawSphere(float radius, int levelOfDetail); */
 private:
        static GLUquadric *m_Quadric;
        static const int MIN_LEVEL_OF_DETAIL;
        static const int MAX_LEVEL_OF_DETAIL;
        
        /* VBO ids for vertex and normal data, respectively */
        /* static GLuint m_SpherePositionBuffer[bufferObjectSize];
         * static GLuint m_SphereNormalBuffer[bufferObjectSize]; */

        /* NOTE: no longer using LOD */
        /* clip bounds for LOD */
        /* static const int MIN_LOD = 1;
         * static const int MAX_LOD = 20;
         * static const float LOD_PER_PIXEL; */
};

#endif
