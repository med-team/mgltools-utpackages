

/***********************************************************************************/
/*																				   */
/*	  Copyright 2003 University of Texas at Austin                                 */
/*	  Authors: Dr C Bajaj bajaj@cs.utexas.edu,                                     */
/*             S K Vinay  skvinay@cs.utexas.edu                                    */
/*             Anthony Thane thanea@ices.utexas.edu                                */
/*																				   */
/*    This program is free software; you can redistribute it and/or modify         */
/*    it under the terms of the GNU General Public License as published by         */
/*    the Free Software Foundation; either version 2 of the License, or            */
/*    (at your option) any later version.                                          */
/*																				   */
/*    This program is distributed in the hope that it will be useful,              */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*    GNU General Public License for more details.                                 */
/*																				   */
/*    You should have received a copy of the GNU General Public License			   */
/*    along with this program; if not, write to the Free Software                  */
/*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA    */
/*                                                                                 */
/***********************************************************************************/
// PlainSphereRenderer.h: interface for the PlainSphereRenderer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PLAINSPHERERENDERER_H__EADD46E7_06F6_4BEF_B26C_21081B2E64CC__INCLUDED_)
#define AFX_PLAINSPHERERENDERER_H__EADD46E7_06F6_4BEF_B26C_21081B2E64CC__INCLUDED_

#include <Cg/cgGL.h>

namespace CCVImposterRenderer {

	class Texture;

	class PlainSphereRenderer  
	{
	public:
		PlainSphereRenderer();
		virtual ~PlainSphereRenderer();

		bool bindProgramAndParams();
		bool unbindProgramAndParams();
		bool initCG();
		bool loadProgram();
		void bindMatrices();

	protected:
		
		CGprofile m_VertexProfile;
		CGprofile m_FragmentProfile;

		CGprogram m_VertexProgram;
		CGprogram m_FragmentProgram;
		CGparameter m_PositionParam;
		CGparameter m_ColorParam;
		CGparameter m_ModelViewITParam;
		CGparameter m_ModelViewInverseParam;
		CGparameter m_ModelViewProjParam;
		CGparameter m_ProjParam;

		CGparameter m_MainColorParam;
		CGparameter m_NormalMapParam;
		CGparameter m_DepthMapParam;
		Texture* m_TextureNormalMap;
		Texture* m_TextureDepthMap;

	};

};

#endif // !defined(AFX_PLAINSPHERERENDERER_H__EADD46E7_06F6_4BEF_B26C_21081B2E64CC__INCLUDED_)
