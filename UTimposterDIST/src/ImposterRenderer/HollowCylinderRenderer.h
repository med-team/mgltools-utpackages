/*****************************************************************************/
/*                             ______________________                        */
/*                            / _ _ _ _ _ _ _ _ _ _ _)                       */
/*            ____  ____  _  / /__  __  _____  __                            */
/*           (_  _)( ___)( \/ /(  \/  )(  _  )(  )                           */
/*             )(   )__)  )  (  )    (  )(_)(  )(__                          */
/*            (__) (____)/ /\_)(_/\/\_)(_____)(____)                         */
/*            _ _ _ _ __/ /                                                  */
/*           (___________/                     ___  ___                      */
/*                                      \  )| |   ) _ _|\   )                */
/*                                 ---   \/ | |  / |___| \_/                 */
/*                                                       _/                  */
/*                                                                           */
/*   Copyright (C) The University of Texas at Austin                         */
/*                                                                           */
/*     Author:     Vinay Siddavanahalli <skvinay@cs.utexas.edu>   2004-2005  */
/*                                                                           */
/*     Principal Investigator: Chandrajit Bajaj <bajaj@ices.utexas.edu>      */
/*                                                                           */
/*         Professor of Computer Sciences,                                   */
/*         Computational and Applied Mathematics Chair in Visualization,     */
/*         Director, Computational Visualization Center (CVC),               */
/*         Institute of Computational Engineering and Sciences (ICES)        */
/*         The University of Texas at Austin,                                */
/*         201 East 24th Street, ACES 2.324A,                                */
/*         1 University Station, C0200                                       */
/*         Austin, TX 78712-0027                                             */
/*         http://www.cs.utexas.edu/~bajaj                                   */
/*                                                                           */
/*         http://www.ices.utexas.edu/CVC                                    */
/*  This software comes with a license. Using this code implies that you     */
/*  read, understood and agreed to all the terms and conditions in that      */
/*  license.                                                                 */
/*                                                                           */
/*  We request that you agree to acknowledge the use of the software that    */
/*  results in any published work, including scientific papers, films and    */
/*  videotapes by citing the reference listed below                          */
/*                                                                           */
/*    C. Bajaj, P. Djeu, V. Siddavanahalli, A. Thane,                        */
/*    Interactive Visual Exploration of Large Flexible Multi-component       */
/*    Molecular Complexes,                                                   */
/*    Proc. of the Annual IEEE Visualization Conference, October 2004,       */
/*    Austin, Texas, IEEE Computer Society Press, pp. 243-250.               */
/*                                                                           */
/*****************************************************************************/
// HollowCylinderRenderer.h: interface for the HollowCylinderRenderer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HOLLOWCYLINDERRENDERER_H__022231BF_C467_4D2B_B5CC_006AB4B39E62__INCLUDED_)
#define AFX_HOLLOWCYLINDERRENDERER_H__022231BF_C467_4D2B_B5CC_006AB4B39E62__INCLUDED_

#include <Cg/cgGL.h>
#include <stdio.h>
#include "Texture.h"
#include "ExpandableBuffer.h"
#include "MyExtensions.h"

namespace CCVImposterRenderer {

	class HollowCylinderRenderer  
	{
	public:
		HollowCylinderRenderer( MyExtensions* extensions );
		virtual ~HollowCylinderRenderer();

		void clear();
		inline void add(float x1, float y1, float z1, float x2, float y2, float z2, float radius, float red, float green, float blue);
		int getNum() const;
		void initRenderer( bool drawFunction );
		void renderBuffer( bool useHardwareBuffer, bool useFunctionOnSurface, double* rotations, double* translations, int numberOfTransformations, bool randomizeColors );
                static void forceMeshRendering(bool status) { m_UseGLFallback = status; }
	protected:
		bool initCG(bool drawFunction);
		bool initExtensions();
		void initDefaults( MyExtensions* extensions );
		void renderOnce();

                bool m_Initialized;
                static bool m_UseGLFallback;

		// profiles
		CGprofile m_VertexProfile;
		CGprofile m_FragmentProfile;

		CGprogram m_VertexProgram;
		CGparameter m_ModelViewITParam;
		CGparameter m_ModelViewInverseParam;
		CGparameter m_ModelViewProjParam;
		CGparameter m_ModelViewParam;
		CGparameter m_ProjParam;

		CGprogram m_FragmentProgram;
		CGparameter m_MainColorParam;
		CGparameter m_NormalMapParam;
		CGparameter m_DepthMapParam;
		CGparameter m_ModelViewProjInverseParam;

		CGparameter m_CylinderDCs;
		CGparameter m_EndPoint1;
		CGparameter m_LengthOfCylinder;

		Texture* m_TextureNormalMap;
		Texture* m_TextureDepthMap;

		// the buffers
		ExpandableBuffer<GLfloat> m_TextureCoord1;
		ExpandableBuffer<GLfloat> m_TextureCoord2;
		ExpandableBuffer<GLfloat> m_Position;
		ExpandableBuffer<GLfloat> m_Color;

		unsigned int m_NumberOfCylinders;
		MyExtensions* m_Extensions;
	};

	inline void HollowCylinderRenderer::add(float x1, float y1, float z1, float x2, float y2, float z2, float radius, float red, float green, float blue)
	{
		radius /= 2.0;
	/*	{
			static int addedTwo = 0;
			if( addedTwo == 2 ) return;
			addedTwo++;
			double shift = 7;
			if( addedTwo == 1 )
			{
				x1 = 13.0f-shift; y1 = -5.0f+shift-4; z1 = 15.0f;
				x2 = 4.0f-shift; y2 = 0.0f+shift-4; z2 = 1.0f;
				red = 1.0;
				green = 0.0;
				blue = 0.0;
			}
			else
			{
				x1 = 10.0f-shift; y1 = 0.0f+shift-4; z1 = 25.0f;
				x2 = 7.0f-shift; y2 = 0.0f+shift-4; z2 = -10.0f;
				red = 0.0;
				green = 1.0;
				blue = 0.0;
			}
			radius = 2.0f;
		}
	*/
		bool result = true;
		// position - stores the offsets
		result &= m_Position.add(0);
		result &= m_Position.add(-1);
		result &= m_Position.add(radius);
		result &= m_Position.add(1);

		result &= m_Position.add(0);
		result &= m_Position.add(1);
		result &= m_Position.add(radius);
		result &= m_Position.add(1);

		result &= m_Position.add(1);
		result &= m_Position.add(1);
		result &= m_Position.add(radius);
		result &= m_Position.add(1);

		result &= m_Position.add(1);
		result &= m_Position.add(-1);
		result &= m_Position.add(radius);
		result &= m_Position.add(1);

		// texturecoord - stores the 2 end points
		result &= m_TextureCoord1.add(x1);
		result &= m_TextureCoord1.add(y1);
		result &= m_TextureCoord1.add(z1);
		result &= m_TextureCoord1.add(1);
		result &= m_TextureCoord1.add(x1);
		result &= m_TextureCoord1.add(y1);
		result &= m_TextureCoord1.add(z1);
		result &= m_TextureCoord1.add(1);
		result &= m_TextureCoord1.add(x1);
		result &= m_TextureCoord1.add(y1);
		result &= m_TextureCoord1.add(z1);
		result &= m_TextureCoord1.add(1);
		result &= m_TextureCoord1.add(x1);
		result &= m_TextureCoord1.add(y1);
		result &= m_TextureCoord1.add(z1);
		result &= m_TextureCoord1.add(1);

		result &= m_TextureCoord2.add(x2);
		result &= m_TextureCoord2.add(y2);
		result &= m_TextureCoord2.add(z2);
		result &= m_TextureCoord2.add(1);
		result &= m_TextureCoord2.add(x2);
		result &= m_TextureCoord2.add(y2);
		result &= m_TextureCoord2.add(z2);
		result &= m_TextureCoord2.add(1);
		result &= m_TextureCoord2.add(x2);
		result &= m_TextureCoord2.add(y2);
		result &= m_TextureCoord2.add(z2);
		result &= m_TextureCoord2.add(1);
		result &= m_TextureCoord2.add(x2);
		result &= m_TextureCoord2.add(y2);
		result &= m_TextureCoord2.add(z2);
		result &= m_TextureCoord2.add(1);

		// color
		result &= m_Color.add(red);
		result &= m_Color.add(green);
		result &= m_Color.add(blue);
		result &= m_Color.add(0.4f);

		result &= m_Color.add(red);
		result &= m_Color.add(green);
		result &= m_Color.add(blue);
		result &= m_Color.add(0.0f);

		result &= m_Color.add(red);
		result &= m_Color.add(green);
		result &= m_Color.add(blue);
		result &= m_Color.add(0.0f);

		result &= m_Color.add(red);
		result &= m_Color.add(green);
		result &= m_Color.add(blue);
		result &= m_Color.add(0.4f);

		if (!result) {
			printf("Warning, could not add Hollow Cylinder");
		}

		m_NumberOfCylinders++;
	}

};

#endif // !defined(AFX_HOLLOWCYLINDERRENDERER_H__022231BF_C467_4D2B_B5CC_006AB4B39E62__INCLUDED_)
