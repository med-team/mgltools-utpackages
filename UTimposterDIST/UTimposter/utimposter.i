%module utimposterrend

%init %{
  import_array(); /* load the Numeric PyCObjects */
%}

%{
#include "numpy/arrayobject.h"

static PyArrayObject *contiguous_typed_array(PyObject *obj, int typecode,
                                      int expectnd, int *expectdims)
{
  PyArrayObject *arr;
  int i;
  char buf[255];

  /* if the shape and type are OK, this function increments the reference
     count and arr points to obj */
  if((arr = (PyArrayObject *)PyArray_ContiguousFromObject(obj,
                                                          typecode, 0,
                                                          10)) == NULL)
    {
      sprintf(buf,"Failed to make a contiguous array of type %d\n", typecode);
      PyErr_SetString(PyExc_ValueError, buf);
      return NULL;
    }

  if(expectnd>0)
    {
      if(arr->nd > expectnd + 1 || arr->nd < expectnd)
        {
          Py_DECREF((PyObject *)arr);
          PyErr_SetString(PyExc_ValueError,
                          "Array has wrong number of dimensions");
          return NULL;
        }
      if(arr->nd == expectnd + 1)
        {
          if(arr->dimensions[arr->nd - 1] != 1)
            {
              Py_DECREF((PyObject *)arr);
              PyErr_SetString(PyExc_ValueError,
                              "Array has wrong number of dimensions");
              return NULL;
            }
        }
      if(expectdims)
        {
          for(i = 0; i < expectnd; i++)
            if(expectdims[i]>0)
              if(expectdims[i] != arr->dimensions[i])
                {
                  Py_DECREF((PyObject *)arr);
                  sprintf(buf,"The extent of dimension %d is %d while %d was expected\n",
                          i, arr->dimensions[i], expectdims[i]);
                  PyErr_SetString(PyExc_ValueError, buf);
                  return NULL;
                }
                  
        }
    }

  return arr;
}

%}

%{
#include "ImposterRenderer.h"
#include "BallRenderer.h"
#include "GaussianRenderer.h"
#include "StickRenderer.h"
#include "HelixRenderer.h"
#include "HollowCylinderRenderer.h"
using namespace CCVImposterRenderer;
  //using CCVImposterRenderer::GaussianRenderer;
  //using CCVImposterRenderer::BallRenderer;
// using CCVImposterRenderer::StickRenderer;
// using CCVImposterRenderer::HollowCylinderRenderer;
//using CCVImposterRenderer::HelixRenderer;

 %}

%include src/ImposterRenderer/BallRenderer.h

%typemap(in) double* (PyArrayObject *array, int expected_dims[1]) 
%{
  if ($input != Py_None)
  {
    expected_dims[0]=0;
    array = contiguous_typed_array($input, PyArray_DOUBLE, 1, expected_dims);
    if (! array) return NULL;
    $1 = (double *)array->data;
  }
  else
  {
    array = NULL;
    $1 = NULL;
  }
%}

%typemap(in) float VECTOR[ANY] (PyArrayObject *array, int expected_dims[1])
%{
  expected_dims[0] = $1_dim0;
  if (expected_dims[0]==1) expected_dims[0]=0;
  array = contiguous_typed_array($input, PyArray_FLOAT, 1, expected_dims);
  if (! array) return NULL;
  $1 = (float *)array->data;
%}

%typemap(freearg) float VECTOR[ANY]

%{
  if ( array$argnum )
      Py_DECREF((PyObject *)array$argnum);
   
%}

%apply float VECTOR[ANY] { float light[4] }
%include src/ImposterRenderer/ImposterRenderer.h

