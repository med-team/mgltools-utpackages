// Copyright (c) 2003,2004  INRIA Sophia-Antipolis (France) and
// Notre Dame University (U.S.A.).  All rights reserved.
//
// This file is part of CGAL (www.cgal.org); you may redistribute it under
// the terms of the Q Public License version 1.0.
// See the file LICENSE.QPL distributed with CGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
// WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
// $Source: /opt/cvs/multisdfDIST/CGAL-3.1/include/CGAL/predicates/Segment_Voronoi_diagram_predicates_C2.h,v $
// $Revision: 1.1.1.1 $ $Date: 2010/03/05 22:18:41 $
// $Name:  $
//
// Author(s)     : Menelaos Karavelas <mkaravel@cse.nd.edu>




#ifndef CGAL_SEGMENT_VORONOI_DIAGRAM_PREDICATES_C2_H
#define CGAL_SEGMENT_VORONOI_DIAGRAM_PREDICATES_C2_H

#include <CGAL/predicates/Segment_Voronoi_diagram_vertex_C2.h>
#include <CGAL/predicates/Svd_compare_x_2.h>
#include <CGAL/predicates/Svd_compare_y_2.h>
#include <CGAL/predicates/Svd_are_parallel_C2.h>
#include <CGAL/predicates/Svd_are_same_points_C2.h>
#include <CGAL/predicates/Svd_are_same_segments_C2.h>
#include <CGAL/predicates/Svd_orientation_C2.h>
#include <CGAL/predicates/Svd_oriented_side_of_bisector_C2.h>
#include <CGAL/predicates/Svd_incircle_2.h>
#include <CGAL/predicates/Svd_finite_edge_interior_2.h>
#include <CGAL/predicates/Svd_infinite_edge_interior_2.h>
#include <CGAL/predicates/Svd_is_degenerate_edge_2.h>
#include <CGAL/predicates/Svd_arrangement_type_C2.h>
#include <CGAL/predicates/Svd_oriented_side_C2.h>

#endif // CGAL_SEGMENT_VORONOI_DIAGRAM_PREDICATES_C2_H
