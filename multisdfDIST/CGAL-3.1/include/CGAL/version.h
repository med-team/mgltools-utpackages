// This file is automatically created by create_internal_release.
// Do not edit manually.

#ifndef CGAL_VERSION_H
#define CGAL_VERSION_H

#define CGAL_VERSION 3.1
#define CGAL_VERSION_NR 1003001100

#endif
