// Copyright (c) 2003,2004  INRIA Sophia-Antipolis (France).
// All rights reserved.
//
// This file is part of CGAL (www.cgal.org); you may redistribute it under
// the terms of the Q Public License version 1.0.
// See the file LICENSE.QPL distributed with CGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
// WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
// $Source: /opt/cvs/multisdfDIST/CGAL-3.1/include/CGAL/Apollonius_graph_short_names_2.h,v $
// $Revision: 1.1.1.1 $ $Date: 2010/03/05 22:18:40 $
// $Name:  $
//
// Author(s)     : Menelaos Karavelas <mkaravel@cse.nd.edu>



#ifndef CGAL_APOLLONIUS_GRAPH_SHORT_NAMES_2_H
#define CGAL_APOLLONIUS_GRAPH_SHORT_NAMES_2_H


#define Apollonius_site_2                          As2
#define Apollonius_graph_2                         Ag2
#define Apollonius_graph_hierarchy_2               Agh2
#define Apollonius_graph_data_structure_2          Agds2
#define Apollonius_graph_vertex_base_2             Agvb2
#define Apollonius_graph_hierarchy_vertex_base_2   Aghvb2
#define Apollonius_graph_traits_2                  Agt2
#define Apollonius_graph_filtered_traits_2         Agft2
#define Apollonius_graph_traits_wrapper_2          Agtw2
#define Apollonius_graph_kernel_wrapper_2          Agkw2


#endif //CGAL_APOLLONIUS_GRAPH_SHORT_NAMES_2_H
