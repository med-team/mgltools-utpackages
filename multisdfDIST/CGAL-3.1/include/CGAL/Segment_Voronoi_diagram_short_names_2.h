// Copyright (c) 2003  INRIA Sophia-Antipolis (France).
// All rights reserved.
//
// This file is part of CGAL (www.cgal.org); you may redistribute it under
// the terms of the Q Public License version 1.0.
// See the file LICENSE.QPL distributed with CGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
// WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
// $Source: /opt/cvs/multisdfDIST/CGAL-3.1/include/CGAL/Segment_Voronoi_diagram_short_names_2.h,v $
// $Revision: 1.1.1.1 $ $Date: 2010/03/05 22:18:41 $
// $Name:  $
//
// Author(s)     : Menelaos Karavelas <mkaravel@cse.nd.edu>



#ifndef CGAL_SEGMENT_VORONOI_DIAGRAM_SHORT_NAMES_2_H
#define CGAL_SEGMENT_VORONOI_DIAGRAM_SHORT_NAMES_2_H


#define Segment_Voronoi_diagram_site_2                    Svds2
#define Segment_Voronoi_diagram_simple_site_2             Svdsims2
#define Segment_Voronoi_diagram_storage_site_2            Svdss2
#define Segment_Voronoi_diagram_simple_storage_site_2     Svdsimss2
#define Segment_Voronoi_diagram_2                         Svd2
#define Segment_Voronoi_diagram_hierarchy_2               Svdh2
#define Segment_Voronoi_diagram_vertex_base_2             Svdvb2
#define Segment_Voronoi_diagram_hierarchy_vertex_base_2   Svdhvb2
#define Segment_Voronoi_diagram_traits_2                  Svdt2
#define Segment_Voronoi_diagram_filtered_traits_2         Svdft2
#define Segment_Voronoi_diagram_traits_wrapper_2          Svdtw2
#define Segment_Voronoi_diagram_kernel_wrapper_2          Svdkw2


#endif //CGAL_SEGMENT_VORONOI_DIAGRAM_SHORT_NAMES_2_H
