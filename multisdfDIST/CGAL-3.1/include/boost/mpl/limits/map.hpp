
#ifndef BOOST_MPL_LIMITS_MAP_HPP_INCLUDED
#define BOOST_MPL_LIMITS_MAP_HPP_INCLUDED

// Copyright Aleksey Gurtovoy 2000-2004
//
// Distributed under the Boost Software License, Version 1.0. 
// (See accompanying file LICENSE_1_0.txt or copy at 
// http://www.boost.org/LICENSE_1_0.txt)
//
// See http://www.boost.org/libs/mpl for documentation.

// $Source: /opt/cvs/multisdfDIST/CGAL-3.1/include/boost/mpl/limits/map.hpp,v $
// $Date: 2010/03/05 22:18:42 $
// $Revision: 1.1.1.1 $

#if !defined(BOOST_MPL_LIMIT_MAP_SIZE)
#   define BOOST_MPL_LIMIT_MAP_SIZE 20
#endif

#endif // BOOST_MPL_LIMITS_MAP_HPP_INCLUDED
