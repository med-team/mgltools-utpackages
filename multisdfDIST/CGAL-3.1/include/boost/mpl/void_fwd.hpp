
#ifndef BOOST_MPL_VOID_FWD_HPP_INCLUDED
#define BOOST_MPL_VOID_FWD_HPP_INCLUDED

// Copyright Aleksey Gurtovoy 2001-2004
//
// Distributed under the Boost Software License, Version 1.0. 
// (See accompanying file LICENSE_1_0.txt or copy at 
// http://www.boost.org/LICENSE_1_0.txt)
//
// See http://www.boost.org/libs/mpl for documentation.

// $Source: /opt/cvs/multisdfDIST/CGAL-3.1/include/boost/mpl/void_fwd.hpp,v $
// $Date: 2010/03/05 22:18:41 $
// $Revision: 1.1.1.1 $

#include <boost/mpl/aux_/adl_barrier.hpp>

BOOST_MPL_AUX_ADL_BARRIER_NAMESPACE_OPEN

struct void_;

BOOST_MPL_AUX_ADL_BARRIER_NAMESPACE_CLOSE
BOOST_MPL_AUX_ADL_BARRIER_DECL(void_)

#endif // BOOST_MPL_VOID_FWD_HPP_INCLUDED
