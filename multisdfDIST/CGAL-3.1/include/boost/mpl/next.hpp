
#ifndef BOOST_MPL_NEXT_HPP_INCLUDED
#define BOOST_MPL_NEXT_HPP_INCLUDED

// Copyright Aleksey Gurtovoy 2004
//
// Distributed under the Boost Software License, Version 1.0. 
// (See accompanying file LICENSE_1_0.txt or copy at 
// http://www.boost.org/LICENSE_1_0.txt)
//
// See http://www.boost.org/libs/mpl for documentation.

// $Source: /opt/cvs/multisdfDIST/CGAL-3.1/include/boost/mpl/next.hpp,v $
// $Date: 2010/03/05 22:18:41 $
// $Revision: 1.1.1.1 $

#include <boost/mpl/next_prior.hpp>

#endif // BOOST_MPL_NEXT_HPP_INCLUDED
