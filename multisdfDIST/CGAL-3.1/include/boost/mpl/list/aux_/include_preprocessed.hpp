
// Copyright Aleksey Gurtovoy 2001-2004
//
// Distributed under the Boost Software License, Version 1.0. 
// (See accompanying file LICENSE_1_0.txt or copy at 
// http://www.boost.org/LICENSE_1_0.txt)
//
// See http://www.boost.org/libs/mpl for documentation.

// $Source: /opt/cvs/multisdfDIST/CGAL-3.1/include/boost/mpl/list/aux_/include_preprocessed.hpp,v $
// $Date: 2010/03/05 22:18:42 $
// $Revision: 1.1.1.1 $

// NO INCLUDE GUARDS, THE HEADER IS INTENDED FOR MULTIPLE INCLUSION!

#include <boost/preprocessor/cat.hpp>
#include <boost/preprocessor/stringize.hpp>

#   define AUX778076_HEADER \
    aux_/preprocessed/plain/BOOST_MPL_PREPROCESSED_HEADER \
/**/

#   include BOOST_PP_STRINGIZE(boost/mpl/list/AUX778076_HEADER)

#   undef AUX778076_HEADER

#undef BOOST_MPL_PREPROCESSED_HEADER
