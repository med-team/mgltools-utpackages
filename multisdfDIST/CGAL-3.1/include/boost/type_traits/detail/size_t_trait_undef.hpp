
// NO INCLUDE GUARDS, THE HEADER IS INTENDED FOR MULTIPLE INCLUSION

// Copyright Aleksey Gurtovoy 2002-2004
//
// Distributed under the Boost Software License, Version 1.0. 
// (See accompanying file LICENSE_1_0.txt or copy at 
// http://www.boost.org/LICENSE_1_0.txt)

// $Source: /opt/cvs/multisdfDIST/CGAL-3.1/include/boost/type_traits/detail/size_t_trait_undef.hpp,v $
// $Date: 2010/03/05 22:18:41 $
// $Revision: 1.1.1.1 $

#undef BOOST_TT_AUX_SIZE_T_TRAIT_DEF1
#undef BOOST_TT_AUX_SIZE_T_TRAIT_SPEC1
#undef BOOST_TT_AUX_SIZE_T_TRAIT_PARTIAL_SPEC1_1
