#ifndef TCOCONE_H
#define TCOCONE_H

#include "datastruct.h"
#include "util.h"
#include "robust_cc.h"

void 
compute_poles( Triangulation &triang);

void 
mark_flat_vertices( Triangulation &triang,
		    double ratio, double cocone_phi, double flat_phi);

void
tcocone(const double DEFAULT_ANGLE,
        const double DEFAULT_SHARP,
	const double DEFAULT_FLAT,
	const double DEFAULT_RATIO,
	Triangulation &triang);


#endif // TCOCONE_H

