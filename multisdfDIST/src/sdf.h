#ifndef SDF_H
#define SDF_H

#include "mds.h"
#include "matrix.h"
#include "kdtree.h"
#include "util.h"

float
sdf( const Point& q, const Mesh &mesh, const vector<double>& weights,
     KdTree& kd_tree, const Triangulation& triang, double maxval=10, double minval=-10);

#endif // SDF_H

