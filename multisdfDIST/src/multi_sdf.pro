TEMPLATE	= app
LANGUAGE	= C++

CONFIG	+= warn_off opengl link_prl console
CONFIG  -= qt

LIBS += ../VolMagick/libVolMagick.a -lCGAL

QMAKE_CXXFLAGS += $$(CPPFLAGS)
QMAKE_LFLAGS += $$(LDFLAGS)

# gmp
#unix:LIBS += -lgmpxx
		
  
linux-g++ {
        INCLUDEPATH += $(CGALINCLUDE)
        INCLUDEPATH += $(CGALCONFIG)
}
linux-g++-64 {
        INCLUDEPATH += $(CGALINCLUDE)
        INCLUDEPATH += $(CGALCONFIG)
}

INCLUDEPATH += /workspace/jgsun/usr/local/include \
	       /usr/include \
	       ../VolMagick
HEADERS	+= 

SOURCES	+=      main.C \
		mesh_io.C \
		sdf.C \
		kdtree.C \
		matrix.C \
		dt.C \
		init.C \
		robust_cc.C \
		rcocone.C \
		tcocone.C \
		op.C \
		util.C \

TARGET  += msdf

contains( QMAKE_CXXFLAGS_RELEASE, -fno-exceptions ) {
        # have to enable exceptions
        QMAKE_CXXFLAGS_RELEASE += -fexceptions
}
