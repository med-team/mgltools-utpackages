#ifndef RCOCONE_H
#define RCOCONE_H

#include "datastruct.h"
#include "util.h"
#include "robust_cc.h"

void
robust_cocone(const double bb_ratio,
	      const double theta_ff,
	      const double theta_if,
	      Triangulation &triang,
	      const char *outfile_prefix);

#endif // RCOCONE_H

